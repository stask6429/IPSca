
def cut(content, begin, end):
    idx1 = content.find(begin)
    idx2 = content.find(end, idx1)
    return content[idx1+len(begin):idx2].strip()

def prepare_html(response):
    try:
        server = response.headers['Server']
    except KeyError:
        server = 'Unknown'
    title = cut(response.text, '<title>', '</title>') \
        if 'JavaScript' not in cut(response.text, '<title>', '</title>') and 'title' in response.text else ''
    h1 = cut(response.text, '<h1>', '</h1>') \
        if 'JavaScript' not in cut(response.text, '<title>', '</title>') and 'h1' in response.text else ''
    return (f'{server} {title}\n{h1}')[:150]